<?php
	session_start();
	
	if(empty($_SESSION['current_user'])){
		header('Location: login.php');
	}else{
		$_SESSION['Registros'] = 1;
	}


?>	
<html>
<head>
    <title>info</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet.css">

</head>
<body>
	<div class="menu">
		<div class="item1-info">
			<a href="info.php">Home</a>
		</div>
		<div class="item2-info">
			<a href="formulario.php">Registrar Alumno</a>
		</div>
		<div class="item3-info">
			<a href="login.php">Cerrar Sesión</a>
		</div>
		<div class="item4-info">
			<a href="destruir_sesion.php">Destruir/borrar Sesión</a>
		</div>
	</div>
	
	<form action="validaciones.php?accion=get&texto=textoenget" method="POST" class="container-formulario">

		<!-- Número de cuenta -->
		<label class="form-label-formulario" for="input-cuenta" id="cuenta-form">Número de cuenta</label>
		<input name="num_cta" class="form-input-formulario" type="integer" id="num_cta-input-form"
			   placeholder="(Max. 10 dígitos)">		

		<!-- Nombre completo -->
		<label class="form-label-formulario" for="input-text" id="nombre-form">Nombre</label>
		<input name="nombre" class="form-input-formulario" type="text" id="nombre-input-form" placeholder="Nombre(s)">
		<label class="form-label-formulario" for="input-text" id="apellido1-form">Primer apellido</label>
		<input name="primer_apellido" class="form-input-formulario" type="text" id="primer_apellido-input-form" placeholder="Primer apellido">
		<label class="form-label-formulario" for="input-text" id="apellido2-form">Segundo apellido</label>
		<input name="segundo_apellido" class="form-input-formulario" type="text" id="segundo_apellido-input-form" placeholder="Segundo apellido (opcional)">


		<!-- Genero -->
		<label class="form-label-formulario" id="genero-form">Género</label>
		<label class="form-radio" id="hombre">
			<input type="radio" name="genero" value="M" checked>
			<i class="form-icon"></i> Hombre
		</label>
		<label class="form-radio" id="mujer">
			<input type="radio" name="genero" value="F">
			<i class="form-icon"></i> Mujer
		</label>
		<label class="form-radio" id="otro">
			<input type="radio" name="genero" value="O">
			<i class="form-icon"></i> Otro
		</label>


		<!-- Fecha de nacimiento -->
		<label class="form-label-formulario" for="input-date" id="fecha-form">Fecha de nacimiento</label>
		<input name="fec_nac" class="form-input-formulario" type="date" id="date-input-form"
			   placeholder="dd/mm/aaaa">


		<!-- Contraseña -->
		<label class="form-label-formulario" for="input-password" id="contra-form">Contraseña</label>
		<input name="contrasena" class="form-input-formulario" type="password" id="password-input-form"
			   placeholder="password">

		
		<div class='container-result-registro'> 
			<?php 
				if(isset($_SESSION['error_registro'])){
					if($_SESSION['error_registro'] == 1){
						echo "<div class='registro-fallido'> Registro fallido. </div>";
						
					//--Número de Cuenta
					if($_SESSION['num_cta-error'] != 0){
						$current_atributo = $_SESSION['current_num_cta'];
						echo "<div class='campo-fallo'>[Número de cuenta]: $current_atributo   </div>";
					}
					if($_SESSION['num_cta-error'] == 3){
						echo "<div class='descripcion-fallo'>El número de cuenta, ya ha sido registrado para otro alumno.</div>";
					}elseif($_SESSION['num_cta-error'] == 1){
						echo "<div class='descripcion-fallo'>El número de cuenta es un campo obligatorio.</div>";
					}elseif($_SESSION['num_cta-error'] == 2){
						echo "<div class='descripcion-fallo'>Solo puede contener números enteros positivos (Máximo 10 digitos)</div>";
					}

					//--Nombre
					if($_SESSION['nombre-error'] != 0){
						$current_atributo = $_SESSION['current_nombre'];
						echo "<div class='campo-fallo'>[Nombre]: $current_atributo   </div>";
					}
					if($_SESSION['nombre-error'] == 1){
						echo "<div class='descripcion-fallo'>El nombre es un campo obligatorio.</div>";
					}elseif($_SESSION['nombre-error'] == 2){
						echo "<div class='descripcion-fallo'>El nombre no puede contener números ni carácteres especiales (por ejemplo: !#$%&/()=?¡*+[]{},)</div>";
					}

					//--Primer Apellido
					if($_SESSION['primer_apellido-error'] != 0){
						$current_atributo = $_SESSION['current_primer_apellido'];
						echo "<div class='campo-fallo'>[Primer apellido]: $current_atributo   </div>";
					}
					if($_SESSION['primer_apellido-error'] == 1){
						echo "<div class='descripcion-fallo'>El primer apellido es un campo obligatorio.</div>";
					}elseif($_SESSION['primer_apellido-error'] == 2){
						echo "<div class='descripcion-fallo'>El primer apellido no puede contener números ni carácteres especiales (por ejemplo: !#$%&/()=?¡*+[]{},)</div>";
					}

					//--Segundo Apellido
					if($_SESSION['segundo_apellido-error'] != 0){
						$current_atributo = $_SESSION['current_segundo_apellido'];
						echo "<div class='campo-fallo'>[Segundo apellido]: $current_atributo   </div>";
					}
					if($_SESSION['segundo_apellido-error'] == 2){
						echo "<div class='descripcion-fallo'>El segundo apellido no puede contener números ni carácteres especiales (por ejemplo: !#$%&/()=?¡*+[]{},)</div>";
					}


					//--Fecha nacimiento
					if($_SESSION['fec_nac-error'] != 0){
						echo "<div class='campo-fallo'>[Fecha de nacimiento]   </div>";
						echo "<div class='descripcion-fallo'>La fecha de nacimiento es un campo obligatorio.</div>";
					}
					
					//--Fecha nacimiento
					if($_SESSION['contrasena-error'] == 1){
						echo "<div class='campo-fallo'>[Contraseña]   </div>";
						echo "<div class='descripcion-fallo'>La contraseña es un campo obligatorio.</div>";
					}


					echo "<div class='registro-fallido-end'> </div>";
					echo "<div class='registro-fallido-end2'> </div>";
					}elseif($_SESSION['error_registro'] == 0){
					echo "<div class='registro-exitoso'> Alumno registrado exitosamente, puede observar todos los alumnos registrados en Home. </div>";
					}
				}
				
				unset($_SESSION['error_registro']);
			?>			
		</div>
		
		<!-- Botones -->
		<input type='submit' class="btn" value="Enviar" id="boton"/>
	</form>

</body>
</html>