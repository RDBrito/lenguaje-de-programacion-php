<?php
	session_start();
	
	if(empty($_SESSION['current_user'])){
		header('Location: login.php');
	}else{
		$_SESSION['Registros'] = 1;
	}


?>	
<html>
<head>
    <title>info</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet.css">

</head>
<body>
	<div class="menu">
		<div class="item1-info">
			<a href="info.php">Home</a>
		</div>
		<div class="item2-info">
			<a href="formulario.php">Registrar Alumno</a>
		</div>
		<div class="item3-info">
			<a href="login.php">Cerrar Sesión</a>
		</div>
		<div class="item4-info">
			<a href="destruir_sesion.php">Destruir/borrar Sesión</a>
		</div>
	</div>



	<div class="display-info">
		<div id="titulo1-info">Usuario Autenticado</div>
		<div class="item5-info"><img src="images/user.png"></div>
		<div class="item6-info">
			<?php
				foreach($_SESSION['Alumno'] as $alumno) {
					if($alumno['num_cta'] == $_SESSION['current_user']){
						$alumno_actual = $alumno;
						$nombre = $alumno_actual['nombre'];
						$primer_apellido = $alumno_actual['primer_apellido'];
						echo "$nombre  $primer_apellido </br>";
					}
				}
			?>
		</div>
		<div class="item7-info">
			<?php
				$num_cta = $alumno_actual['num_cta'];
				$segundo_apellido = $alumno_actual['segundo_apellido'];
				$genero = $alumno_actual['genero'];
				$fec_nac = $alumno_actual['fec_nac'];
				echo "Número de cuenta: $num_cta </br>";
				echo "Nombre: $nombre  $primer_apellido $segundo_apellido </br>";
				if($genero == 'M'){
					echo "Genero: Hombre  </br>";
				}elseif($genero == 'F'){
					echo "Genero: Mujer  </br>";
				}else{
					echo "Genero: Otro  </br>";
				}
				echo "Fecha de nacimiento: $fec_nac  </br>";

			?>
		</div>
	</div>
	<div class="display-registros">
		<div class="atributo" id="item8-info">
			#
		</div>
		<div class="atributo" id="item9-info">
			Nombre
		</div>
		<div class="atributo" id="item10-info">
			Genero
		</div>
		<div class="atributo" id="item11-info">
			Fecha de nacimiento
		</div>
		
		<?php foreach($_SESSION['Alumno'] as $alumno):?>
		
		<div class="atributo-valor" id="item12-info">
			<?php 
				$num_cta = $alumno['num_cta']; 
				echo "$num_cta </br>";
			?>
		</div>
		<div class="atributo-valor" id="item13-info">
			<?php 
				$nombre = $alumno['nombre'];
				$primer_apellido = $alumno['primer_apellido'];
				$segundo_apellido = $alumno['segundo_apellido'];
				echo "$nombre  $primer_apellido $segundo_apellido </br>";
			?>
		</div>
		<div class="atributo-valor" id="item14-info">
			<?php 
				$genero = $alumno['genero'];
				if($genero == 'M'){
					echo "Hombre  </br>";
				}elseif($genero == 'F'){
					echo "Mujer  </br>";
				}else{
					echo "Otro  </br>";
				}				
			?>
		</div>
		<div class="atributo-valor" id="item15-info">
			<?php 
				$fec_nac = $alumno['fec_nac']; 
				echo "$fec_nac </br>";
			?>
		</div>
		<?php endforeach; ?> 
	</div>
</body>
</html>