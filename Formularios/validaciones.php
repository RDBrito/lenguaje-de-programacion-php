<?php
	session_start();
	
	if(empty($_SESSION['current_user'])){
		header('Location: login.php');
	}
	
	$_SESSION['num_cta-error'] = 0;
	$_SESSION['nombre-error'] = 0;
	$_SESSION['primer_apellido-error'] = 0;
	$_SESSION['segundo_apellido-error'] = 0;
	$_SESSION['fec_nac-error'] = 0;
	$_SESSION['contrasena-error'] = 0;
	
	
	
    echo "<br /> POST:";
    print_r($_POST);
	
	
	$_SESSION['current_num_cta'] = $_POST['num_cta'];
	if(empty($_POST['num_cta'])){
		$_SESSION['num_cta-error'] = 1; //No ingreso nada en el campo
	}else{
		$result = preg_match('/^([0-9]{1,10}$)/' , $_SESSION['current_num_cta']);
		if($result == 0){
			$_SESSION['num_cta-error'] = 2; //No ingreso solo enteros o la longitud es mayor a 10 digitos
		}		
	}
	
	foreach($_SESSION['Alumno'] as $alumno){
		if($alumno['num_cta'] == $_POST['num_cta']){
			$_SESSION['num_cta-error'] = 3;  //Ya existe alguien mas con ese número de cuenta
		}
	}
	



	$_SESSION['current_nombre'] = $_POST['nombre'];
	if(empty($_POST['nombre'])){
		$_SESSION['nombre-error'] = 1; //No ingreso nada en el campo
	}else{
		$result = preg_match('/^((([a-zA-ZáéíóúÁÉÍÓÚñÑü])+( )*)+$)/' , $_SESSION['current_nombre']);
		if($result == 0){
			$_SESSION['nombre-error'] = 2; //No corresponde a un nombre
		}		
	}


	$_SESSION['current_primer_apellido'] = $_POST['primer_apellido'];
	if(empty($_POST['primer_apellido'])){
		$_SESSION['primer_apellido-error'] = 1; //No ingreso nada en el campo
	}else{
		$result = preg_match('/^((([a-zA-ZáéíóúÁÉÍÓÚñÑü])+( )*)+$)/' , $_SESSION['current_primer_apellido']);
		if($result == 0){
			$_SESSION['primer_apellido-error'] = 2; //No corresponde a un apellido
		}		
	}	
	

	$_SESSION['current_segundo_apellido'] = $_POST['segundo_apellido'];
	if(!(empty($_POST['segundo_apellido']))){
		$result = preg_match('/^((([a-zA-ZáéíóúÁÉÍÓÚñÑü])+( )*)+$)/' , $_SESSION['current_segundo_apellido']);
		if($result == 0){
			$_SESSION['segundo_apellido-error'] = 2; //No corresponde a un apellido
		}
	}
	

	$_SESSION['current_fec_nac'] = $_POST['fec_nac'];
	if(empty($_POST['fec_nac'])){
		$_SESSION['fec_nac-error'] = 1; //No ingreso nada en el campo
	}


	if(empty($_POST['contrasena'])){
		$_SESSION['contrasena-error'] = 1; //No ingreso nada en el campo
	}

	

	$_SESSION['error_registro'] = 0;
	if($_SESSION['num_cta-error'] != 0){
		$_SESSION['error_registro'] = 1;
	}else{
		if($_SESSION['nombre-error'] != 0){
			$_SESSION['error_registro'] = 1;
		}else{
			if($_SESSION['primer_apellido-error'] != 0){
				$_SESSION['error_registro'] = 1;
			}else{
				if($_SESSION['segundo_apellido-error'] != 0){
					$_SESSION['error_registro'] = 1;
				}else{
					if($_SESSION['fec_nac-error'] != 0){
						$_SESSION['error_registro'] = 1;
					}else{
						if($_SESSION['contrasena-error'] != 0){
							$_SESSION['error_registro'] = 1;
						}
					}
				}
			}
		}
	}

	if($_SESSION['error_registro'] == 1){
		echo "<br> Registro fallido";
	}else{
		$originalDate = $_POST['fec_nac'];
		$newDate = date("d-m-Y", strtotime($originalDate));
		$_POST['fec_nac'] = $newDate;
		array_push($_SESSION['Alumno'], $_POST);
		echo "<br> Registro exitoso";

	}
	
	header('Location: formulario.php');
	

?>	

