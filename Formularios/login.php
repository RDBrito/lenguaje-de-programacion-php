<?php
	session_start();

//Pre cargando al usuario administrador inicial	
	if(!(isset($_SESSION['Registros']))){
		$_SESSION['Alumno'] = [
			0 => [
				'num_cta' => '1',
				'nombre' => 'Admin',
				'primer_apellido' => 'General',
				'segundo_apellido' => '',
				'contrasena' => 'adminpass123.',
				'genero' => 'O',
				'fec_nac' => '25-01-1990'
			]
			
		];
	}	

?>	
<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet.css">

</head>
<body>
	<div class="fondo">
		<div class="container">
			<header id="header-login">Login</header>
			<div class="item1"><img src="images/icono_persona.png"></div>
			<form action="validar_login.php?accion=get&texto=textoenget" method="POST" class="item2">

				<!-- Número de cuenta -->
				<label class="form-label-login" for="input-cuenta">Número de cuenta</label>
				<input name="num_cta" class="form-input-login" type="number" id="input-num_cta"
					   placeholder="(solo números enteros)">

				<!-- Contraseña -->
				<label class="form-label-login" for="input-password">Contraseña</label>
				<input name="contrasena" class="form-input-login" type="password" id="input-password"
					   placeholder="password">
				<?php 
					if(isset($_SESSION['Error'])){
						if($_SESSION['Error'] == 1){
							echo "<div id='error'> Usuario y/o contraseña inválida </div>";
						}
					}
				?>

				<!-- Boton -->
				<input type='submit' class="btn1" value="Enviar"/>
			</form>
		</div>
	</div>
</body>
</html>