<?php
	session_start();
	
?>	
<html>
<head>
    <title>Probar expresiones regulares</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">

</head>
<body>
	<div class="titulo">
		<div id="titulo-principal">Probando expresiones regulares</div>
	</div>
	
	<form action="expresiones_regulares.php?accion=get&texto=textoenget" method="POST" class="container-formulario">

		<!-- Nombre completo -->
		<div id="item1">
			<p class="ejercicio">1. Realizar una expresión regular que detecte emails correctos.</p>
			<p class="expresion">Expresion regular: </p>
			<p class="expresion">'/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/'</p>
			<?php 
				if(isset($_SESSION['invalid_email'])){
					$cadena = $_SESSION['email'];
					if($_SESSION['invalid_email'] == 1){
						echo "<div class='incorrecto'> email: $cadena <br> inválido </div>";
					}else{
						echo "<div class='correcto'> email: $cadena <br> válido </div>";
					}
				}
			?>
		</div>
		<input name="email" class="inputs" type="text" id="input-email" placeholder="cadena a validar">


		<div id="item2">
			<p class="ejercicio">2. Realizar una expresión regular que detecte Curps Correctos</p>
			<p class="especificacion">- es decir que permita usar únicamente estos caracteres: ABCD123456EFGHIJ78.</p>
			<p class="expresion">Expresion regular: </p>
			<p class="expresion">'/^[A-Z]{4}+[0-9]{6}+[A-Z]{6}+[0-9]{2}$/'</p>
			<?php 
				if(isset($_SESSION['invalid_curp'])){
					$cadena = $_SESSION['curp'];
					if($_SESSION['invalid_curp'] == 1){
						echo "<div class='incorrecto'> CURP: $cadena <br> inválido </div>";
					}else{
						echo "<div class='correcto'> CURP: $cadena <br> válido </div>";
					}
				}
			?>
		</div>
		<input name="curp" class="inputs" type="text" id="input-curp" placeholder="cadena a validar">


		<div id="item3">
			<p class="ejercicio">3. Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.</p>
			<p class="expresion">Expresion regular: </p>
			<p class="expresion">'/^(([a-zA-Z áéíóúÁÉÍÓÚñÑü]{50})([a-zA-Z áéíóúÁÉÍÓÚñÑü])*$)/'</p>
			<?php 
				if(isset($_SESSION['invalid_palabra50'])){
					$cadena = $_SESSION['palabra50'];
					if($_SESSION['invalid_palabra50'] == 1){
						echo "<div class='incorrecto'> Palabra: $cadena <br> inválido </div>";
					}else{
						echo "<div class='correcto'> Palabra: $cadena <br> válido </div>";
					}
				}
			?>
		</div>
		<input name="palabra50" class="inputs" type="text" id="input-palabra50" placeholder="cadena a validar">


		<div id="item4">
			<p class="ejercicio">4. Crea una función para escapar los símbolos especiales.</p>
			<p class="expresion">Expresion regular: </p>
			<p class="expresion">funcion en archivo "expresiones_regulares.php" lineas 43-50</p>
			<?php 
				if(isset($_SESSION['cadena'])){
					$cadena = $_SESSION['cadena'];
					$ingresada = $_SESSION['ingresada'];
					echo "<div class='escape'> Cadena ingresada: <br> $ingresada </div>";
					echo "<div class='escape'> Cadena sin símbolos especiales: <br> $cadena </div>";
				}
			?>
		</div>
		<input name="simbolos" class="inputs" type="text" id="input-simbolos" placeholder="cadena">


		<div id="item5">
			<p class="ejercicio">5. Crear una expresión regular para detectar números decimales.</p>
			<p class="expresion">Expresion regular: </p>
			<p class="expresion">'/^((-){0,1}([0-9]+)(.)([0-9])+$)/'</p>
			<?php 
				if(isset($_SESSION['invalid_decimal'])){
					$cadena = $_SESSION['decimal'];
					if($_SESSION['invalid_decimal'] == 1){
						echo "<div class='incorrecto'> Número decimal: $cadena <br> inválido (si el número no tiene parte entera colocar un 0, ej: 0.5) </div>";
					}else{
						echo "<div class='correcto'> Número decimal: $cadena <br> válido </div>";
					}
				}
			?>
		</div>
		<input name="decimal" class="inputs" type="text" id="input-decimal" placeholder="cadena a validar">

		
		<!-- Botones -->
		<input type='submit' class="btn" value="Verificar" id="boton"/>
	</form>

</body>
</html>