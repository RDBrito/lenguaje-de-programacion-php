<?php

//---- Validaciones recibiendo las cadenas desde index.php ------------------------------------------//
	
	
	session_start();
	print_r($_POST);
	$_SESSION['invalid_email'] = 0;
	$_SESSION['invalid_curp'] = 0;
	$_SESSION['invalid_palabra50'] = 0;

	$_SESSION['invalid_decimal'] = 0;
	
	echo "<br><br> -----  Si es 1 es inválido  -----<br>";
	
	echo "<br> probando correo con post <br>";
	$_SESSION['email'] = $_POST['email'];
	$result = preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/' , $_SESSION['email']);
	if($result == 0){
		$_SESSION['invalid_email'] = 1;
	}
	echo $_SESSION['invalid_email'];

	echo "<br> probando CURP con post <br>";
	$_SESSION['curp'] = $_POST['curp'];
	$result = preg_match('/^[A-Z]{4}+[0-9]{6}+[A-Z]{6}+[0-9]{2}$/' , $_SESSION['curp']);
	if($result == 0){
		$_SESSION['invalid_curp'] = 1;
	}
	echo $_SESSION['invalid_curp'];


	echo "<br> probando palabras50 con post <br>";
	$_SESSION['palabra50'] = $_POST['palabra50'];
	$result = preg_match('/^(([a-zA-Z áéíóúÁÉÍÓÚñÑü]{50})([a-zA-Z áéíóúÁÉÍÓÚñÑü])*$)/' , $_SESSION['palabra50']);
	if($result == 0){
		$_SESSION['invalid_palabra50'] = 1;
	}
	echo $_SESSION['invalid_palabra50'];


	echo "<br> funcion <br>";
	function escapa_simbolos($cadena){
		$new_string = "";
		$claves = preg_split('/[^0-9a-zA-Z áéíóúñÁÉÍÓÚÑ \s]+/', $cadena);
		foreach($claves as $fragmento){
			$new_string .= $fragmento; 
		}
		return $new_string;
	}
	$_SESSION['ingresada'] = $_POST['simbolos'];
	$_SESSION['cadena'] = escapa_simbolos($_SESSION['ingresada']);
	echo $_SESSION['cadena'];


	echo "<br> probando número decimal con post <br>";
	$_SESSION['decimal'] = $_POST['decimal'];
	$result = preg_match('/^((-){0,1}([0-9]+)(.)([0-9])+$)/' , $_SESSION['decimal']);
	if($result == 0){
		$_SESSION['invalid_decimal'] = 1;
	}
	echo $_SESSION['invalid_decimal'];

	header('Location: index.php');


//------------------------------------------------------------------------------------------------------//
	
	echo "<br> <br> <br>";	

	

/*----------------------------------------------------------------------------------------------------------
       Para probar la axpreciones regulares solo con este archivo, comentar las lineas 3 a 67
                	   asignar las cadenas a probra en las variables $check
----------------------------------------------------------------------------------------------------------*/	
	
	echo "<br><br> --------  Si es 1 es válido  --------<br>";
	
	
	$check = "diego.brito@outlook.com";
	echo "<br> probando email: $check <br>";
	$result = preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/' , $check);
	echo $result;
	echo "<br>";
	
	$check = "ABCD123456EFGHIJ78";
	echo "<br> probando CURP: $check <br>";
	$result = preg_match('/^[A-Z]{4}+[0-9]{6}+[A-Z]{6}+[0-9]{2}$/' , $check);
	echo $result;
	echo "<br>";
	
	
	//  '/^(([a-zA-Z]{50})([a-zA-Z])*$)/'	
	$check = "qwertyuioPqwertyuioPqwertyuioPqwertyuioPqwertyuioP";
	echo "<br> Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.";
	echo "<br> Palabra: $check<br>";
	$result = preg_match('/^(([a-zA-Z áéíóúÁÉÍÓÚñÑü]{50})([a-zA-Z áéíóúÁÉÍÓÚñÑü])*$)/' , $check);
	echo $result;	
	echo "<br>";

	
	$check = "(Ho)la, m{e_ l!#-lamo _[*+Di/e/g#o";
	echo "<br> Probanddo función para escapar los símbolos especiales";
	function escapa_simbolos2($cadena){
		$new_string = "";
		$claves = preg_split('/[^0-9a-zA-Z áéíóúñÁÉÍÓÚÑ \s]+/', $cadena);
		foreach($claves as $fragmento){
			$new_string .= $fragmento; 
		}
		return $new_string;
	}	
	$new_string = escapa_simbolos2($check);
	echo "<br> Ingresada: $check";
	echo "<br> Resultado: $new_string";
	echo "<br>";
	

	$check = "3.1415";
	echo "<br> Probando número decimal: $check";
	echo "<br> (si el número no tiene parte entera colocar un 0, ej: 0.5) <br>";
	$result = preg_match('/^((-){0,1}([0-9]+)(.)([0-9])+$)/' , $check);
	echo $result;	
	echo "<br>";
?>	